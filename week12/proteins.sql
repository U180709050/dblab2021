select * from Proteins;

explain select * from Proteins where pid like "5HT2C_HUMA%";
explain select * from Proteins where accession like "Q9UBA6";

create unique index idx1 using btree on Proteins(pid);
alter table Proteins add constraint acc_pk primary key (accession);

alter table Proteins drop index idx1;
alter table Proteins drop primary key;