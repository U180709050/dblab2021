show table status;
show tables;

create temporary table products_below_awg
select productid, productname, price
from Products
where price < (select avg(price) from Products);
drop table products_below_awg;

select * from products_below_awg;